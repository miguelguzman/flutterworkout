# Semana 5

Aplicación de Paises (Continuación 3)

## Descripción

Aplicación que despliega una pantalla con dos vistas (Continuación de branch.semana4)
Se presentó una mejora en:

- El menú de navegación: Cambiamos los botonos inferiores por un BottomNavbar nativo.
- Diálogo de detalle: Mejoramos la presentación de la información con nuevos estilos de texto.
- Diálogo de carga en Lista de países: Incluímos un background con gradiente.
- Validación de formulario: Implementamos validaciones sobre los inputs del formulario de registro.
- Formulario: Se implementó la carga de los valores de la moneda y de ubicación por medio de un diálogo.
- Se integró Firebase como base de datos.
- Se cargaron los datos desde el webservice inicial a Firebase.
- Se permite almacenar los valores del formulario en la base de datos.