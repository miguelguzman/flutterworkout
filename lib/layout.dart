import 'package:flutter/material.dart';

import 'package:semana_5/screens/country_loader.dart';
import 'package:semana_5/screens/register_country.dart';
import 'package:semana_5/utilities/strings.dart';

class Layout extends StatefulWidget {

  String title;
  Widget mainContent;

  Layout({Key? key, required this.title, required this.mainContent}) : super(key: key);

  @override
  State<Layout> createState() => _LayoutState(title: this.title, mainContent: this.mainContent);

}

class _LayoutState extends State<Layout> {

  String title = '';
  int index = 0;
  Widget? mainContent;

  _LayoutState ({required this.title, this.mainContent, this.index = 0});

  void _onItemTapped(int i) {

    setState(() {

      index = i;
      mainContent = CountryLoaderScreen();
      title = StringsUtils.country_list;

      switch (index) {

        case 1:

          mainContent = RegisterCountryScreen();
          title = StringsUtils.register_country;
          break;

      }

    });

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title == '' ? StringsUtils.country_list : title),
      ),
      body: Center(
        child: mainContent ?? CountryLoaderScreen(),
      ),
      bottomNavigationBar: BottomNavigationBar(
        selectedItemColor: Colors.blue,
        iconSize: 40,
        items: <BottomNavigationBarItem>[
          _getNavBtn(0),
          _getNavBtn(1),
        ],
        currentIndex: index,
        onTap: _onItemTapped,
      ),
    );
  }

  BottomNavigationBarItem _getNavBtn (final int index) {

    Icon icon = Icon(Icons.list_alt_rounded);
    String text = StringsUtils.country_list;

    switch (index) {

      case 1:
        icon = Icon(Icons.details_rounded);
        text = StringsUtils.register_country;
        break;

    }

    return BottomNavigationBarItem(
      icon: icon,
      label: text,
    );

  }

}