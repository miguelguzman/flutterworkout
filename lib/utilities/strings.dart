class StringsUtils {

  static const String app = 'Países',

      list = 'Listado',
      country_list = 'Lista de países',
      country_detail = 'Detalle del país',
      register_country = 'Registrar país',
      name = 'Nombre',
      flag = 'Bandera',
      currency = 'Moneda',
      latitude = 'Latitud',
      longitude = 'Longitud',
      save = 'Guardar',
      ok = 'Ok',
      cancel = 'Cancelar',
      current_currency_is = 'Su moneda actual es',
      abbr = 'Abreviatura',
      symbol = 'Símbolo',
      add_currency = 'Agregar moneda',
      remove = 'Quitar',
      location = 'Ubicación',
      add_location = 'Agregar ubicación',
      code = 'Código',

      country_succesfuly_saved = 'País almacenado exitosamente',

      there_are_some_errors = 'Hay algunos errores presentes',
      error_load_countries = 'Hubo un error cargando los países',
      error_could_not_load_countries = 'No se pudo cargar los países',
      error_must_insert_name = 'Debe ingresar un nombre de país',
      error_must_insert_flag = 'Debe ingresar la url de la bandera',
      error_must_insert_currency = 'Debe ingresar la moneda',
      error_must_insert_latitude = 'Debe ingresar la latitud',
      error_must_insert_longitude = 'Debe ingresar la longitud',
      error_must_insert_abbr = 'Debe ingresar la abreviatura de la moneda',
      error_must_insert_symbol = 'Debe ingresar el símbolo de la moneda',
      error_must_set_currency = 'Debe darle un valor de moneda al país',
      error_must_set_location = 'Debe darle un valor de ubicación del país',
      error_latitude_must_be_numeric = 'La latitud debe ser un valor numérico',
      error_longitude_must_be_numeric = 'La longitud debe ser un valor numérico',
      error_must_insert_code = 'Debe ingresar un código';

}