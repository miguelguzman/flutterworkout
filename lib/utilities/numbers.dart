import 'package:semana_5/utilities/constants.dart';

class NumbersUtils {

  static bool isNumeric(String s) {

    try {

      var v1 = double.tryParse(s);

      if (Constants.DEBUG)
        print('v1: ${v1}');

      return v1 != null;

    } catch (e) {

      try {

        var v2 = int.tryParse(s);

        if (Constants.DEBUG)
          print('v2: ${v2}');

        return v2 != null;

      } catch (e1) {}

    }

    return false;

  }

}