import 'package:flutter/material.dart';

class CustomInput extends StatelessWidget {

  final String label, errorText;
  final TextEditingController? controller;

  const CustomInput ({Key? key, required this.label, required this.errorText, this.controller}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return TextField(
      controller: controller ?? null,
      decoration: InputDecoration(
        border: UnderlineInputBorder(),
        labelText: label,
      ),
    );

  }

}

class CustomFormInput extends StatelessWidget {

  final String label, errorText;
  final TextEditingController? controller;

  const CustomFormInput ({Key? key, required this.label, required this.errorText, this.controller}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return TextFormField(
      controller: controller ?? null,
      decoration: InputDecoration(
        border: UnderlineInputBorder(),
        labelText: label,
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return errorText;
        }
        return null;
      },
    );

  }

}