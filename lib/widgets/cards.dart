import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';

class CustomCard extends StatelessWidget {

  final Widget child;
  final double padding, radius, elevation;

  CustomCard ({required this.child, this.padding = 10, this.radius = 10, this.elevation = 10});

  @override
  Widget build(BuildContext context) {

    return Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(radius)),
      elevation: elevation,
      child: Padding(
        padding: EdgeInsets.all(padding),
        child: child,
      ),
    );

  }

}