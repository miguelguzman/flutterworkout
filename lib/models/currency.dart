import 'dart:convert';

class Currency {

  String abbr = '',
      name = '';

  String? symbol = '';

  Currency ({required this.abbr, required this.name, this.symbol});

  Currency.fromJson(Map<String, dynamic> json) {

    if (json != null)
      for (String a in json.keys) {

        abbr = a;
        name = json[a]['name'] != null ? json[a]['name'] : null;
        symbol = json[a]['symbol'] != null ? json[a]['symbol'] : null;

      }

  }

  Map<String, dynamic> toJson() => {
    'abbr': abbr,
    'name': name,
    'symbol': symbol,
  };

  String toString() {

    return jsonEncode(toJson());

  }

}