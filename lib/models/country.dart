import 'dart:convert';

import 'package:semana_5/models/currency.dart';

class Country {

  String code = '',
      name = '',
      flag = '';

  double latitude = 0.0,
      longitude = 0.0;

  Currency? currency = null;

  Country ({this.code = '', this.name = '', this.flag = '', this.latitude = 0.0, this.longitude = 0.0, this.currency});

  Country.fromDocumentSnapshot (String c, Map<String, dynamic> json) {

    code = c;
    name = json['name'];
    flag = json['flag'];
    latitude = double.parse(json['latitude'].toString());
    longitude = double.parse(json['longitude'].toString());
    currency = Currency.fromJson(json['currencies']);

  }

  Country.fromJson(Map<String, dynamic> json) {

    if (json.containsKey('cca3'))
      code = json['cca3'];

    if (json.containsKey('translations'))
      name = json['translations']['spa']['common'] + ' (' + json['translations']['spa']['official'] + ')';

    if (json.containsKey('flags'))
      flag = json['flags']['png'];

    if (json.containsKey('latlng')) {

      latitude = json['latlng'][0];
      longitude = json['latlng'][1];

    }

    try {

      if (json.containsKey('currencies')) {

        currency = Currency.fromJson(json['currencies']);

      }

    } catch (e) {

      print(e);
      print(json['currencies']);

    }

  }

  Map<String, dynamic> toJson() => {
    'code': code,
    'name': name,
    'flag': flag,
    'currency': currency,
    'latitude': latitude,
    'longitude': longitude,
  };

  Map<String, dynamic> toJsonFirebase () {

    return {
      'name':name,
      'flag':flag,
      'currencies':{
        currency?.abbr: {
          'name': currency?.name,
          'symbol': currency?.symbol
        }
      },
      'latitude':latitude,
      'longitude':longitude
    };

  }

  String toString() {

    return jsonEncode(toJson());

  }

}