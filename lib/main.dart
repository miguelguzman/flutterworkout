import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';

import 'package:semana_5/layout.dart';
import 'package:semana_5/screens/country_loader.dart';
import 'package:semana_5/utilities/strings.dart';
import 'package:semana_5/firebase_options.dart';

void main() {

  WidgetsFlutterBinding.ensureInitialized();
  Firebase.initializeApp().then((value) {

    runApp(const CountryApp());

  });

}

class CountryApp extends StatelessWidget {

  const CountryApp ({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: StringsUtils.app,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialRoute: '/home',
      routes: {
        '/home': (context) => Layout(title: StringsUtils.country_list, mainContent: CountryLoaderScreen()),
      },
    );

  }

}