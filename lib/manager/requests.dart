import 'dart:async';
import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:http/http.dart' as http;
import 'package:semana_5/models/country.dart';
import 'package:semana_5/utilities/strings.dart';

class Requests {

  static final String getCountries = 'https://restcountries.com/v3.1/subregion/south';

}

class RequestManager {

  static final String _GET = 'GET',
      _POST = 'POST';

  static Future<http.Response> _query (final String url, final Object? params, final String? auth, final String method) {

    final Map<String, String>? headers = auth == null ? null : {'authorization': 'Basic ${auth}'};

    if (method == _POST)
      return http.post(Uri.parse(url), headers: headers, body: params);

    return http.get(Uri.parse(url), headers: headers);

  }

  static Future<http.Response> get (final String url, final String? auth) {

    return _query(url, null, auth, _GET);

  }

  static Future<http.Response> post (final String url, final Object? params, final String? auth) {

    return _query(url, params, auth, _POST);

  }

  static Future<List<Country>> getCountries () async {

    try {

      final List<Country> countries = [];

      final http.Response response = await get(Requests.getCountries, null);

      if (response.statusCode == 200) {

        final List<dynamic> cs = jsonDecode(utf8.decode(response.bodyBytes));

        if (cs != null)
          for (Map<String, dynamic> c in cs)
            if (c != null) {

              final Country country = Country.fromJson(c);
              putCountryOnFirebase(country, null, null);
              countries.add(country);

            }

        countries.sort((a, b) => a.name.compareTo(b.name));

        return countries;

      } else {

        print(StringsUtils.error_load_countries);
        return [];

      }

    } catch (e) {

      print(e);
      return [];

    }

  }

  static Future<List<Country>> getCountriesFromFirebase () async {

    final CollectionReference countriesReference = FirebaseFirestore.instance.collection('countries');

    QuerySnapshot countriesQS = await countriesReference.get();

    if (countriesQS != null)
      if (countriesQS.docs != null)
        if (countriesQS.docs.length > 0) {

          final List<Country> countries = [];

          for (var c in countriesQS.docs) {

            final Map<String, dynamic> m = c.data() as Map<String, dynamic>;
            countries.add(Country.fromDocumentSnapshot(c.id, m));

          }

          return countries;

        }

    return [];

  }

  static Future<void> putCountryOnFirebase (final Country country, Function(dynamic)? onSuccess, Function(dynamic)? onError) async {

    final CollectionReference countriesReference = FirebaseFirestore.instance.collection('countries');

    onSuccess = (onSuccess == null) ? (value) {

      print("value: $value");

    } : onSuccess;

    onError = (onError == null) ? (error) {

      print("error: $error");

    } : onError;

    countriesReference
        .doc(country.code)
        .set(country.toJsonFirebase(), SetOptions(merge: true))
        .then(onSuccess)
        .catchError(onError);

  }

}