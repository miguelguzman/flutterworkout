import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';

class LoaderScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    return Stack(
      alignment: Alignment.center,
      children: [
        //Background
        Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                Colors.white,
                Colors.blueAccent,
              ],
            ),
          ),
        ),
        //Indicator
        CircularProgressIndicator(),
      ],
    );

  }

}