import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:semana_5/models/currency.dart';
import 'package:semana_5/utilities/constants.dart';
import 'package:semana_5/utilities/numbers.dart';
import 'package:semana_5/utilities/strings.dart';
import 'package:semana_5/widgets/input.dart';

class LatLonDialog extends StatefulWidget {

  const LatLonDialog ({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => LatLonDialogState();

}

class LatLonDialogState extends State<LatLonDialog> {

  final _formKey = GlobalKey<FormState>();

  final _latitudeControler = TextEditingController(),
      _longitudeControler = TextEditingController();

  @override
  void dispose () {

    _latitudeControler.dispose();
    _longitudeControler.dispose();
    super.dispose();

  }

  @override
  Widget build(BuildContext context) {

    return AlertDialog(
      content: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(0),
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                CustomInput(label: StringsUtils.latitude, errorText: StringsUtils.error_must_insert_latitude, controller: _latitudeControler,),
                CustomInput(label: StringsUtils.longitude, errorText: StringsUtils.error_must_insert_longitude, controller: _longitudeControler,),
              ],
            ),
          ),
        ),
      ),
      actions: <Widget>[
        TextButton(
          child: Text(StringsUtils.ok),
          onPressed: () {

            print('_formKey: ${!_formKey.currentState!.validate()}\n_validateForm: ${!_validateForm(context)}');

            if (!_formKey.currentState!.validate() || !_validateForm(context))
              return;

            Navigator.of(context).pop({'longitude': _longitudeControler.text, 'latitude': _latitudeControler.text});

          },
        ),
        TextButton(
          child: Text(StringsUtils.cancel),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      ],
    );

  }

  bool _validateForm (context) {

    if (!NumbersUtils.isNumeric(_latitudeControler.text)) {

      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text(StringsUtils.error_latitude_must_be_numeric)),
      );
      return false;

    }

    if (!NumbersUtils.isNumeric(_longitudeControler.text)) {

      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text(StringsUtils.error_longitude_must_be_numeric)),
      );
      return false;

    }

    return true;

  }

}