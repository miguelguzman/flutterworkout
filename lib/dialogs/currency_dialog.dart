import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:semana_5/models/currency.dart';
import 'package:semana_5/utilities/constants.dart';
import 'package:semana_5/utilities/strings.dart';
import 'package:semana_5/widgets/input.dart';

class CurrencyDialog extends StatefulWidget {

  const CurrencyDialog ({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => CurrencyDialogState();

}

class CurrencyDialogState extends State<CurrencyDialog> {

  final _formKey = GlobalKey<FormState>();

  final _nameControler = TextEditingController(),
      _abbrControler = TextEditingController(),
      _symbolControler = TextEditingController();

  @override
  void dispose () {

    _nameControler.dispose();
    _abbrControler.dispose();
    _symbolControler.dispose();
    super.dispose();

  }

  @override
  Widget build(BuildContext context) {

    return AlertDialog(
      content: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(0),
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                CustomInput(label: StringsUtils.name, errorText: StringsUtils.error_must_insert_name, controller: _nameControler,),
                CustomInput(label: StringsUtils.abbr, errorText: StringsUtils.error_must_insert_abbr, controller: _abbrControler,),
                CustomInput(label: StringsUtils.symbol, errorText: StringsUtils.error_must_insert_symbol, controller: _symbolControler,),
              ],
            ),
          ),
        ),
      ),
      actions: <Widget>[
        TextButton(
          child: Text(StringsUtils.ok),
          onPressed: () {

            if (!_formKey.currentState!.validate() || !_validateForm(context))
              return;

            Navigator.of(context).pop(Currency(abbr: _abbrControler.text, name: _nameControler.text, symbol: _symbolControler.text));

          },
        ),
        TextButton(
          child: Text(StringsUtils.cancel),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      ],
    );

  }

  bool _validateForm (context) {

    if (_nameControler.text == null || _nameControler.text == '') {

      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text(StringsUtils.error_must_insert_name)),
      );
      return false;

    }

    if (_abbrControler.text == null || _abbrControler.text == '') {

      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text(StringsUtils.error_must_insert_abbr)),
      );
      return false;

    }

    return true;

  }

}