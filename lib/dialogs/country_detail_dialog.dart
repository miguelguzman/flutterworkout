import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:semana_5/models/country.dart';
import 'package:semana_5/utilities/strings.dart';

class CountryDetailDialog extends StatelessWidget {

  final Country country;

  const CountryDetailDialog ({Key? key, required this.country}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return AlertDialog(
      content: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(0),
          child: _CountryDetailDialogContent(country: country),
        ),
      ),
      actions: <Widget>[
        TextButton(
          child: Text(StringsUtils.ok),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      ],
    );

  }

}

class _CountryDetailDialogContent extends StatelessWidget {

  final Country country;

  const _CountryDetailDialogContent ({Key? key, required this.country}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Text(country.name, maxLines: 4,style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),),
        Container( height: 30, ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  _CountryDetailDialogContentItem(icon: Icons.payment_outlined, text: '${StringsUtils.currency}: ${country.currency!.name} (${country.currency!.symbol})'),
                  _CountryDetailDialogContentItem(icon: Icons.location_on_outlined, text: '${StringsUtils.latitude}: ${country.latitude}'),
                  _CountryDetailDialogContentItem(icon: Icons.location_on_outlined, text: '${StringsUtils.longitude}: ${country.longitude}'),
                ],
              ),
            ),
            SizedBox(width: 5,),
            SizedBox(width: 70, child: Image.network(country.flag),),
          ],
        )
      ],
    );

  }

}

class _CountryDetailDialogContentItem extends StatelessWidget {

  final IconData icon;
  final String text;

  const _CountryDetailDialogContentItem ({Key? key, required this.icon, required this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return Row(
      children: [
        Icon(icon, color: Colors.blue,),
        SizedBox(width: 10,),
        Flexible(
          child: Text(text, style: const TextStyle(color: Colors.black38),),
        ),
      ],
    );

  }

}