import 'package:flutter/material.dart';
import 'package:semana_5/dialogs/currency_dialog.dart';
import 'package:semana_5/dialogs/latlon_dialog.dart';
import 'package:semana_5/manager/requests.dart';
import 'package:semana_5/models/country.dart';
import 'package:semana_5/models/currency.dart';
import 'package:semana_5/utilities/strings.dart';
import 'package:semana_5/widgets/cards.dart';
import 'package:semana_5/widgets/input.dart';

class RegisterCountryScreen extends StatefulWidget {

  const RegisterCountryScreen ({Key? key}) : super(key: key);

  @override
  State<RegisterCountryScreen> createState() => _RegisterCountryScreenState();

}

class _RegisterCountryScreenState extends State<RegisterCountryScreen> {

  final _formKey = GlobalKey<FormState>();
  Currency? _currency = null;
  double? _latitude = null, _longitude = null;
  bool _currencyVisibility = false, _locationVisibility = false;

  final _codeControler = TextEditingController(),
      _nameControler = TextEditingController(),
      _flagControler = TextEditingController();

  @override
  void dispose () {

    _codeControler.dispose();
    _nameControler.dispose();
    _flagControler.dispose();
    super.dispose();

  }

  @override
  Widget build(BuildContext context) {

    return Padding(
      padding: EdgeInsets.all(10),
      child: Form(
        key: _formKey,
        child: Center(
          child: ListView(
            children: <Widget>[
    CustomInput(label: StringsUtils.code, errorText: StringsUtils.error_must_insert_code, controller: _codeControler,),
    CustomInput(label: StringsUtils.name, errorText: StringsUtils.error_must_insert_name, controller: _nameControler,),
              CustomInput(label: StringsUtils.flag, errorText: StringsUtils.error_must_insert_flag, controller: _flagControler,),
              CustomCard(
                //child: CustomInput(label: StringsUtils.currency, errorText: StringsUtils.error_must_insert_currency),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Visibility(
                      visible: _currencyVisibility,
                      child: Row(
                        children: [
                          Expanded(
                            flex: 1,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: [
                                Text(StringsUtils.currency, style: TextStyle(color: Colors.blue),),
                                Text(_currency != null ? '${_currency!.name} (${_currency!.abbr})' : ''),
                              ],
                            ),
                          ),
                          ElevatedButton(
                            onPressed: () {

                              setState(() {
                                _currency = null;
                                _currencyVisibility = false;
                              });

                            },
                            child: Text(StringsUtils.remove),
                          ),
                        ],
                      ),
                    ),
                    ElevatedButton(
                      onPressed: () {

                        showDialog(
                            context: context,
                            barrierDismissible: true,
                            builder: (BuildContext context) {
                              return CurrencyDialog();
                            }).then((currency) {

                          currency = currency as Currency;

                          if (currency == null || currency.abbr == null || currency.abbr == ''
                              || currency.name == null || currency.name == '') {

                            ScaffoldMessenger.of(context).showSnackBar(
                              const SnackBar(content: Text(StringsUtils.error_must_set_currency)),
                            );
                            return;

                          }
                          setState(() {
                            _currency = currency;
                            _currencyVisibility = true;
                          });

                          print('result: $currency');

                        });

                      },
                      child: Text(StringsUtils.add_currency),
                    ),
                  ],
                ),
              ),
              CustomCard(
                //child: CustomInput(label: StringsUtils.currency, errorText: StringsUtils.error_must_insert_currency),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Visibility(
                      visible: _locationVisibility,
                      child: Row(
                        children: [
                          Expanded(
                            flex: 1,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: [
                                Text(StringsUtils.location, style: TextStyle(color: Colors.blue),),
                                Text(_latitude != null ? '${StringsUtils.latitude}: ${_latitude}' : ''),
                                Text(_longitude != null ? '${StringsUtils.longitude}: ${_longitude}' : ''),
                              ],
                            ),
                          ),
                          ElevatedButton(
                            onPressed: () {

                              setState(() {
                                _latitude = null;
                                _longitude = null;
                                _locationVisibility = false;
                              });

                            },
                            child: Text(StringsUtils.remove),
                          ),
                        ],
                      ),
                    ),
                    ElevatedButton(
                      onPressed: () {

                        showDialog(
                            context: context,
                            barrierDismissible: true,
                            builder: (BuildContext context) {
                              return LatLonDialog();
                            }).then((value) {

                          if (value == null && (_latitude == null || _longitude == null)) {

                            ScaffoldMessenger.of(context).showSnackBar(
                              const SnackBar(content: Text(StringsUtils.error_must_set_location)),
                            );
                            return;

                          }

                          var lat = double.tryParse(value['latitude']);
                          if (lat == null) {

                            ScaffoldMessenger.of(context).showSnackBar(
                              const SnackBar(content: Text(StringsUtils.error_must_insert_latitude)),
                            );
                            return;

                          }

                          var long = double.tryParse(value['longitude']);
                          if (long == null) {

                            ScaffoldMessenger.of(context).showSnackBar(
                              const SnackBar(content: Text(StringsUtils.error_must_insert_longitude)),
                            );
                            return;

                          }

                          setState(() {
                            _latitude = lat;
                            _longitude = long;
                            _locationVisibility = true;
                          });

                        });

                      },
                      child: Text(StringsUtils.add_location),
                    ),
                  ],
                ),
              ),
              ElevatedButton(
                onPressed: () {

                  if (_formKey.currentState!.validate() && !(_currency == null) && !(_latitude == null || _longitude == null)) {

                    final Country c = Country(
                        code: _codeControler.text,
                        name: _nameControler.text,
                        flag: _flagControler.text,
                        latitude: _latitude ?? 0.0,
                        longitude: _longitude ?? 0.0,
                        currency: _currency
                    );

                    RequestManager.putCountryOnFirebase(c, (data) {

                      ScaffoldMessenger.of(context).showSnackBar(
                        const SnackBar(content: Text(StringsUtils.country_succesfuly_saved)),
                      );

                      clearForm();

                    }, (error) {

                      ScaffoldMessenger.of(context).showSnackBar(
                        const SnackBar(content: Text(StringsUtils.there_are_some_errors)),
                      );

                    });

                  }

                },
                child: Text(StringsUtils.save),
              ),
            ],
          ),
        ),
      ),
    );

  }

  void clearForm () {

    setState(() {
      _codeControler.text = '';
      _nameControler.text = '';
      _flagControler.text = '';
      _latitude = null;
      _longitude = null;
      _currency = null;
      _currencyVisibility = false;
      _locationVisibility = false;
    });

  }

}