import 'package:flutter/material.dart';

import 'package:semana_5/dialogs/country_detail_dialog.dart';
import 'package:semana_5/models/country.dart';

class CountryListScreen extends StatelessWidget {

  final List<Country>? countries;

  const CountryListScreen ({Key? key, this.countries}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return ListView.builder(
        itemCount: countries == null ? 0 : countries!.length,
        itemBuilder: (context, i) {

          Widget widget = countries == null ?
            Center( child: Text('No se pudieron cargar los paises'), ) :
            _CountryItem(country: countries![i],);

          return widget;

        }
    );

  }

}

class _CountryItem extends StatelessWidget {

  final Country country;

  const _CountryItem ({Key? key, required this.country}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return ListTile(
      leading: SizedBox( width: 70, child: Image.network(country.flag), ),
      title: Text( country.name, overflow: TextOverflow.clip, maxLines: 3, ),
      //subtitle: Text( '${country.currency!.name} (${country.currency!.symbol} [${country.currency!.abbr}])', overflow: TextOverflow.clip, maxLines: 1, ),
      onTap: () {
        showDialog<void>(
          context: context,
          barrierDismissible: true,
          builder: (BuildContext context) {
            return CountryDetailDialog(country: country);
          },
        );
      },
    );

  }

}