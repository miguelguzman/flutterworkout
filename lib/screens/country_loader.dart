import 'package:flutter/material.dart';

import 'package:semana_5/loader.dart';
import 'package:semana_5/manager/requests.dart';
import 'package:semana_5/models/country.dart';
import 'package:semana_5/screens/country_list.dart';

class CountryLoaderScreen extends StatefulWidget {

  const CountryLoaderScreen ({Key? key}) : super(key: key);

  @override
  State<CountryLoaderScreen> createState() => _CountryLoaderScreenState();

}

class _CountryLoaderScreenState extends State<CountryLoaderScreen> {

  late Future<List<Country>> countries;

  @override
  void initState() {

    super.initState();
    countries = RequestManager.getCountriesFromFirebase();

  }

  @override
  Widget build(BuildContext context) {

    return FutureBuilder<List<Country>>(
      future: countries,
      builder: (context, snapshot) {

        if (snapshot.hasData) {

          return CountryListScreen(countries: snapshot.data);

        } else if (snapshot.hasError) {

          return Center(
            child: Text('${snapshot.error}'),
          );

        }

        return Center(
          child: LoaderScreen(),
        );

      },
    );

  }

}